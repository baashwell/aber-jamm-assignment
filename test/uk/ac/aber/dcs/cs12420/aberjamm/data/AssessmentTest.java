/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.aber.dcs.cs12420.aberjamm.data;

import java.util.ArrayList;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ben
 */

public class AssessmentTest
{
	private Assessment a;
	private AssessmentPart part;
	private AssessmentPart addPart;
	
	@Before
	public void SetContents()
	{
		a = new Assessment();
		part = new AssessmentPart();
		addPart = new AssessmentPart();
		a.addAssessmentPart(part);
		a.addAssessmentPart(addPart);
	}
	
	@Test
	public void addAssessmentPartTest()
	{		
		AssessmentPart compare = a.getAssessmentPart(0);		
		assertEquals("Assessment Part: ", part, compare);
	}
	
	@Test
	public void getMaximumTest()
	{
		part.setMaximumMark(10);
		addPart.setMaximumMark(10);
		
		int total = a.getMaximum();
		assertEquals("Maximum Marks: ", 20, total);
	}
	
	@Test
	public void typeTest()
	{
		AssessmentType t = AssessmentType.ASSIGNMENT;
		a.setType(t);
		assertEquals("Type Test", t, a.getType());
	}

}
