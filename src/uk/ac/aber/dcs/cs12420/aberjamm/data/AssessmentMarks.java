package uk.ac.aber.dcs.cs12420.aberjamm.data;

//Imports
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

/**
 * Assessment Marks holds the marks for each part of the assessment and can run methods to get the overall mark
 * and give feedback to the user.
 * @author Ben Ashwell
 *
 */
public class AssessmentMarks implements Serializable
{
	//Attributes
	
	/** String to hold the feedback grade*/
	private String feedback;
	/** A vector to hold the list of assignment part marks */
	private ArrayList<AssessmentPartMark> assessmentPartMarks  = new ArrayList<AssessmentPartMark>();
	/** A Assessment variable to hold the assessment linked to this mark*/
	private Assessment assessment;
	
	//Constructors
	
	public AssessmentMarks()
	{
	}
	
	public AssessmentMarks(Assessment assessment)
	{
		this.assessment = assessment;
	}
	
	//Methods
	
	/**
	 * method to add an assessment part mark to the list
	 * @param partMark
	 */
	public void addAssessmentPartMark(AssessmentPartMark partMark)
	{
		assessmentPartMarks.add(partMark);
	}
	/**
	 * Goes through every object in the list of AssessmentPartMarks and get the mark from each one and add them together,
	 * once each one has gone through it returns the float number
	 * @return
	 */
	public int getTotal()
	{
		//temporary int to hold the added value
		int total = 0;
		
		for(int i = 0; i <= assessmentPartMarks.size() - 1; i++)
		{
			//get the current object
			AssessmentPartMark apm = assessmentPartMarks.get(i);
			int mark = apm.getMark();
			total += mark;
		}
		
		return total;
	}
	
	/**
	 * This  method gets the type of feedback wanted then runs through to make a string including the assessment name and
	 *  then followed by the assessment grade in the appropriate type in the correct format.
	 * @param type
	 * @return
	 */
	public String getFeedbackGrade(FeedbackGradeType type)
	{	
		//Write the assessment title with a full stop and space for the grade
		String s = assessment.getName() + ". ";
		
		if(feedback == "O")
		{
			s += "No Assessment Recorded.";
			return s;
		}
		else if(feedback == "I")
		{
			s += "Assessment Incomplete For Valid Reasons.";
			return s;
		}
		else if(feedback == "Z")
		{
			s += "Grade Cancelled For Assessment Offence.";
			return s;
		}
		else
		{
			//Maths to get percentage, using bigdecimal due to the decimal points of division
			BigDecimal i = new BigDecimal(getTotal());		
			BigDecimal max = new BigDecimal(assessment.getMaximum());
			BigDecimal percentage = (i.divide(max, 5, RoundingMode.HALF_UP));
			percentage = percentage.multiply((BigDecimal.valueOf(100)));

			int value = percentage.intValue();

			if(type == FeedbackGradeType.NUMERIC)
			{
				s += "Grade: " + i;
				return s;
			}
			else if(type == FeedbackGradeType.COARSE_GRAIN)
			{


				if(value >= 70)
				{
					s += "A";
				}
				else if(value <70 && value >= 60)
				{
					s += "B";			
				}
				else if(value <60 && value >= 50)
				{
					s += "C";			
				}
				else if(value <50 && value >= 40)
				{
					s += "D";			
				}
				else if(value <40 && value >= 35)
				{
					s += "E";			
				}
				else
				{
					s += "F";			
				}
				return s;
			}
			else
			{			
				if(value >= 96)
				{
					s += "A++";			
				}
				else if(value <96 && value >= 90)
				{
					s += "A+";			
				}
				else if(value <90 && value >= 80)
				{
					s += "A";			
				}
				else if(value <80 && value >= 70)
				{
					s += "A-";			
				}
				else if(value <70 && value >= 67)
				{
					s += "B+";			
				}
				else if(value <67 && value >= 64)
				{
					s += "B";			
				}
				else if(value <64 && value >= 60)
				{
					s += "B-";			
				}
				else if(value <60 && value >= 57)
				{
					s += "C+";			
				}
				else if(value <57 && value >= 54)
				{
					s += "C";			
				}
				else if(value <54 && value >= 50)
				{
					s += "C-";			
				}
				else if(value <50 && value >= 47)
				{
					s += "D+";			
				}
				else if(value <47 && value >= 44)
				{
					s += "D";			
				}
				else if(value <44 && value >= 40)
				{
					s += "D-";			
				}
				else if(value <40 && value >= 35)
				{
					s += "E";			
				}
				else if(value <35 && value >= 31)
				{
					s += "F+";			
				}
				else if(value <31 && value >= 16)
				{
					s += "F";			
				}
				else
				{
					s += "F-";			
				}
				return s;
			}
		}
	}

	/**
	 * @return the feedback
	 */
	public String getFeedback() 
	{
		return feedback;
	}

	/**
	 * @param feedback the feedback to set
	 */
	public void setFeedback(String feedback) 
	{
		this.feedback = feedback;
	}

	/**
	 * @return the assessmentPartMarks
	 */
	public ArrayList<AssessmentPartMark> getAssessmentPartMarks()
	{
		return assessmentPartMarks;
	}

	/**
	 * @param assessmentPartMarks the assessmentPartMarks to set
	 */
	public void setAssessmentPartMarks(ArrayList<AssessmentPartMark> assessmentPartMarks) 
	{
		this.assessmentPartMarks = assessmentPartMarks;
	}

	/**
	 * @return the assessment
	 */
	public Assessment getAssessment() 
	{
		return assessment;
	}

	/**
	 * @param assessment the assessment to set
	 */
	public void setAssessment(Assessment assessment) 
	{
		this.assessment = assessment;
	}
}
