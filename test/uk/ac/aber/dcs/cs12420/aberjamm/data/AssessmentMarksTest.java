package uk.ac.aber.dcs.cs12420.aberjamm.data;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.junit.*;
import uk.ac.aber.dcs.cs12420.aberjamm.data.*;

public class AssessmentMarksTest
{
	Assessment assessment;
	AssessmentMarks am;
	AssessmentPart part;
	AssessmentPart addPart;
	AssessmentPartMark partMark;
	AssessmentPartMark addPartMark;
	
	@Before
	public void setContents() throws InvalidMarkException
	{
		assessment = new Assessment();
		am = new AssessmentMarks(assessment);
		part = new AssessmentPart();
		addPart = new AssessmentPart();
		partMark = new AssessmentPartMark(part);
		addPartMark = new AssessmentPartMark(addPart);
		
		am.addAssessmentPartMark(partMark);
		am.addAssessmentPartMark(addPartMark);
		part.setMaximumMark(10);
		addPart.setMaximumMark(20);
		partMark.setMark(10);
		addPartMark.setMark(3);
		assessment.addAssessmentPart(part);
		assessment.addAssessmentPart(addPart);
	}
	
	@Test
	public void getTotalTest()
	{
		int total = am.getTotal();
		assertEquals("Total marks:", 13, total);
	}
	@Test
	public void getFeedbackGradeTest()
	{
		String cg = am.getFeedbackGrade(FeedbackGradeType.COARSE_GRAIN);
		String fg = am.getFeedbackGrade(FeedbackGradeType.FINE_GRAIN);
		String nu = am.getFeedbackGrade(FeedbackGradeType.NUMERIC);
		
		assertEquals("Feedback Grade:", "default. D", cg);
		assertEquals("Feedback Grade:", "default. D-", fg);
		assertEquals("Feedback Grade:", "default. Grade: 13", nu);
	}
	@Test
	public void assessmentTest()
	{
		am.setAssessment(assessment);
		assertEquals("assessment test", assessment, am.getAssessment());
	}
	@Test
	public void assessmentPartMarksTest()
	{
		ArrayList<AssessmentPartMark> test = am.getAssessmentPartMarks();
		assertEquals("assessmentPartMarks test", partMark, test.get(0));
	}
}
