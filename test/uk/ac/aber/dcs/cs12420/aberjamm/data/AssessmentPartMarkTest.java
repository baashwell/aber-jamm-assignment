package uk.ac.aber.dcs.cs12420.aberjamm.data;

import static org.junit.Assert.*;
import org.junit.*;
import uk.ac.aber.dcs.cs12420.aberjamm.data.*;

public class AssessmentPartMarkTest 
{
	AssessmentPartMark apm;
	AssessmentPart ap;
	
	@Before
	public void setContents()
	{
		ap = new AssessmentPart();
		apm = new AssessmentPartMark(ap);
		
		ap.setMaximumMark(10);
	}
	
	@Test
	public void setMarkTest() throws InvalidMarkException
	{
		apm.setMark(5);
		assertEquals("Mark", 5, apm.getMark());
		
		try
		{
			//Should fail every time.
			apm.setMark(15);
		}
		catch(InvalidMarkException e)
		{
			assertEquals("Mark", 5, apm.getMark());
		}	
	}
	
	@Test
	public void assessmentPartTest()
	{
		apm.setAssessmentPart(ap);
		assertEquals("AssessmentPart test", ap, apm.getAssessmentPart());
	}
}
