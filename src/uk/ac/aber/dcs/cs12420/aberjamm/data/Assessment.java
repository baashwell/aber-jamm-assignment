package uk.ac.aber.dcs.cs12420.aberjamm.data;

//Imports
import java.io.Serializable;
import java.util.Date;
import java.util.ArrayList;

/**
 * Assessment class is the second major part of the Aber Jamm project because it itself holds the parts
 * of the assessment so that it can be linked vi these to the marks and the students.
 * @author Ben Ashwell
 *
 */
public class Assessment implements Serializable
{
	//Attributes
	
	/** String to hold the name of the assessment*/
	private String name;
	/** AssessmentType enum to hold the type of the assessment*/
	private AssessmentType type;
	/** Date to hold the hand out date of the assessment*/
	private Date handOutDate;
	/** Date to hold the hand in date of the assessment*/
	private Date handInDate;
	/** Vector to hold the AssessmentParts for this Assessment */
	private ArrayList<AssessmentPart> assessmentParts = new ArrayList<AssessmentPart>();
	
	//Constructors
	
	public Assessment()
	{
		this("default");
	}
	
	public Assessment(String name)
	{
		this.name = name;
	}
	
	//Methods
	
	/**
	 * Add an AssessmentPart to the AssessmentParts list
	 * @param part
	 */
	public void addAssessmentPart(AssessmentPart part)
	{
		assessmentParts.add(part);
	}
	
	/**
	 * Goes through every object in the list of AssessmentPartMarks and get the mark from each one and add them together,
	 * once each one has gone through convert the total to a string and return it.
	 * @return
	 */
	public int getMaximum()
	{
		//temporary int to hold the added value
		int total = 0;
		
		//have to minus one from size to allow for loop to run correctly
		for(int i = 0; i <= assessmentParts.size() - 1; i++)
		{
			//get the current object
			AssessmentPart apm = assessmentParts.get(i);
			int mark = apm.getMaximumMark();
			total += mark;
		}
		
		return total;
	}
	/**
	 * Method to set the HandInDate
	 * @param date
	 */
	public void setHandInDate(Date date)
	{
		this.handInDate = date;
	}
	/**
	 * Method to set the HandOutDate
	 * @param date
	 */
	public void setHandOutDate(Date date)
	{
		this.handOutDate = date;
	}
	/**
	 * Method to set the Assessment Type
	 * @param date
	 */
	public void setType(AssessmentType type)
	{
		this.type = type;
	}
	/**
	 * Method to get an assessment part at a relevent location
	 * @param index
	 * @return
	 */
	public AssessmentPart getAssessmentPart(int index)
	{
		return assessmentParts.get(index);
	}
	/**
	 * Method to get the name of the assessment
	 * @return
	 */
	public String getName() 
	{
		return name;
	}

	/**
	 * @return the assessmentParts
	 */
	public ArrayList<AssessmentPart> getAssessmentParts()
	{
		return assessmentParts;
	}

	/**
	 * @param assessmentParts the assessmentParts to set
	 */
	public void setAssessmentParts(ArrayList<AssessmentPart> assessmentParts) 
	{
		this.assessmentParts = assessmentParts;
	}

	/**
	 * @return the type
	 */
	public AssessmentType getType()
	{
		return type;
	}

	/**
	 * @return the handOutDate
	 */
	public Date getHandOutDate()
	{
		return handOutDate;
	}

	/**
	 * @return the handInDate
	 */
	public Date getHandInDate() 
	{
		return handInDate;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) 
	{
		this.name = name;
	}
}
