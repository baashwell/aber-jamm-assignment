package uk.ac.aber.dcs.cs12420.aberjamm.data;

import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.*;
import uk.ac.aber.dcs.cs12420.aberjamm.data.*;

public class StudentTest 
{
	private Student s;
	private Assessment a;
	
	@Before
	public void setContents()
	{
		s = new Student();
		a = new Assessment("TestAssessment");
	}
	
	@Test
	public void addAssessmentTest()
	{
		s.addAssessment(a);
		
		ArrayList<AssessmentMarks> aml = s.getAssessmentMarks();
		AssessmentMarks am = aml.get(0);
		Assessment as = am.getAssessment();
		assertEquals("Assessment student link", a.getName(), as.getName());
	}
	
	@Test
	public void AssessmentMarksTest()
	{
		ArrayList<AssessmentMarks> test = new ArrayList<AssessmentMarks>();
		s.setAssessmentMarks(test);
		assertEquals("AssessmentMarks List Test", test, s.getAssessmentMarks());
	}
}
