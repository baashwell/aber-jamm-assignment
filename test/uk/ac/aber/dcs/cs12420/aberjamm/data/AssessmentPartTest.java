package uk.ac.aber.dcs.cs12420.aberjamm.data;

import static org.junit.Assert.*;
import org.junit.*;
import uk.ac.aber.dcs.cs12420.aberjamm.data.*;

public class AssessmentPartTest 
{
	AssessmentPart ap;
	
	@Before
	public void setContents()
	{
		ap = new AssessmentPart();
		ap.setMaximumMark(20);
	}
	
	@Test
	public void isValidTest()
	{
		boolean a = ap.isValid(5);
		assertEquals("True Trial", true, a);
		boolean b = ap.isValid(50);
		assertEquals("False Trial", false, b);		
	}
}
