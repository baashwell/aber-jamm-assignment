package uk.ac.aber.dcs.cs12420.aberjamm.data;

import static org.junit.Assert.*;
import java.io.Serializable;
import java.util.ArrayList;

import org.junit.*;
import uk.ac.aber.dcs.cs12420.aberjamm.data.*;

public class ModuleTest implements Serializable 
{
	private Module w;
	private Module r;
	private Assessment a;
	private Student s;
	private AssessmentPart ap;
	
	@Before
	public void setContents()
	{
		w = new Module("Test");
		r = new Module();
		a = new Assessment("TestAssessment");
		s = new Student("TestStudent");
		ap = new AssessmentPart("TestAssessmentPart", 4);
		w.addAssessment(a);
		a.addAssessmentPart(ap);
		w.setStudents(s);
	}
	
	@Test
	public void writeAndReadTest()
	{
		w.write("test");
		r = r.read("test.xml");
		assertEquals("ModuleName", r.getModuleCode(), w.getModuleCode());
		
		ArrayList<Assessment> al =  r.getAssessments();
		Assessment assessment = al.get(0);
		String an = assessment.getName();		
		assertEquals("AssessmentName", an, a.getName());
		
		AssessmentPart testap = assessment.getAssessmentPart(0);
		String apname = testap.getDescription();
		assertEquals("AssessmentPartDescription", apname, ap.getDescription());
		
		ArrayList<Student> sl = r.getStudents();
		Student tests = sl.get(0);
		String testsname = tests.getStudentReference();
		assertEquals("StudentName", testsname, s.getStudentReference());
	}
	@Test
	public void assessmentsTest()
	{
		ArrayList<Assessment> test = w.getAssessments();
		assertEquals("Assessments Test", a, test.get(0));
	}
	@Test
	public void studentsTest()
	{
		ArrayList<Student> test = w.getStudents();
		assertEquals("Assessments Test", s, test.get(0));
	}
}
