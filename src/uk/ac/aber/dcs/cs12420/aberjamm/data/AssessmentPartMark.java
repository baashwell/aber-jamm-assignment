package uk.ac.aber.dcs.cs12420.aberjamm.data;

import java.io.Serializable;

/**
 * Assessment Part Mark holds the marks for a given assessment part.
 * @author Ben Ashwell
 *
 */
public class AssessmentPartMark implements Serializable
{
	//Attributes
	
	/** integer to hold the mark of this assessment part*/
	private int mark;
	/** A link to the appropriate Assessment Part*/
	private AssessmentPart assessmentPart;
	
	//Constructors
	
	public AssessmentPartMark()
	{
	}
	
	public AssessmentPartMark(AssessmentPart assessmentPart)
	{
		this.assessmentPart = assessmentPart;
	}
	
	//Methods
	
	/**
	 * Sets the mark of this assessment part, if it is a valid mark. Otherwise an exception is thrown.
	 * @param mark
	 * @throws InvalidMarkException
	 */
	public void setMark(int mark) throws InvalidMarkException
	{
		boolean b = assessmentPart.isValid(mark);
		if(b == true)
		{
			this.mark = mark;
		}
		else
		{
			throw new InvalidMarkException(null);
		}
	}
	
	/**
	 * Get the mark attribute for this Assignment Part
	 * @return
	 */
	public int getMark() 
	{
		return mark;
	}

	/**
	 * @return the assessmentPart
	 */
	public AssessmentPart getAssessmentPart() 
	{
		return assessmentPart;
	}

	/**
	 * @param assessmentPart the assessmentPart to set
	 */
	public void setAssessmentPart(AssessmentPart assessmentPart)
	{
		this.assessmentPart = assessmentPart;
	}
	
}
