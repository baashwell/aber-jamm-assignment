package uk.ac.aber.dcs.cs12420.aberjamm.data;

public enum AssessmentType 
{
	ASSIGNMENT,
	EXAM,
	WORKSHEET;
}
