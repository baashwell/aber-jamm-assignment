package uk.ac.aber.dcs.cs12420.aberjamm.data;

//Imports
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Student is a key class in the Aber Jamm Project as it holds the links between the assessments and the marks, as 
 * well as all of the student information.
 * @author Ben Ashwell
 *
 */
public class Student implements Serializable 
{
	//Attributes
	
	/** String to hold the students forenames*/
	private String forenames;
	/** String to hold the students surname*/
	private String surname;
	/** String to hold the students student reference*/
	private String studentReference;
	/** String to hold the students email*/
	private String email;
	/** A boolean to hold the if the student is enrolled*/
	private boolean enrolled;
	/** A vector to hold the list of Assessment Marks */
	private ArrayList<AssessmentMarks> assessmentMarks  = new ArrayList<AssessmentMarks>();
	
	//Constructors
	public Student()
	{
	}
	
	public Student(String studentReference)
	{
		this.studentReference = studentReference;
	}
	
	public Student(String forenames, String surname, String studentReference)
	{
		this.forenames = forenames;
		this.surname = surname;
		this.studentReference = studentReference;
	}
	
	public Student(String forenames, String surname, String studentReference, String email)
	{
		this.forenames = forenames;
		this.surname = surname;
		this.studentReference = studentReference;
		this.email = email;
	}
	
	//methods
	
	public void addAssessment(Assessment assessment)
	{
		assessmentMarks.add(new AssessmentMarks(assessment));
	}

	/**
	 * @return the forenames
	 */
	public String getForenames() 
	{
		return forenames;
	}

	/**
	 * @param forenames the forenames to set
	 */
	public void setForenames(String forenames)
	{
		this.forenames = forenames;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() 
	{
		return surname;
	}

	/**
	 * @param surname the surname to set
	 */
	public void setSurname(String surname)
	{
		this.surname = surname;
	}

	/**
	 * @return the studentReference
	 */
	public String getStudentReference() 
	{
		return studentReference;
	}

	/**
	 * @param studentReference the studentReference to set
	 */
	public void setStudentReference(String studentReference)
	{
		this.studentReference = studentReference;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}

	/**
	 * @return the enrolled
	 */
	public boolean isEnrolled()
	{
		return enrolled;
	}

	/**
	 * @param enrolled the enrolled to set
	 */
	public void setEnrolled(boolean enrolled)
	{
		this.enrolled = enrolled;
	}

	/**
	 * @return the assessmentMarksList
	 */
	public ArrayList<AssessmentMarks> getAssessmentMarks() 
	{
		return assessmentMarks;
	}

	/**
	 * @param assessmentMarksList the assessmentMarksList to set
	 */
	public void setAssessmentMarks(ArrayList<AssessmentMarks> assessmentMarks) 
	{
		this.assessmentMarks = assessmentMarks;
	}
	
	
}
