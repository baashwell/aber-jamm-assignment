package uk.ac.aber.dcs.cs12420.aberjamm.data;

public enum FeedbackGradeType 
{
	COARSE_GRAIN,
	FINE_GRAIN,
	NUMERIC;
}
