package uk.ac.aber.dcs.cs12420.aberjamm.data;

public class InvalidMarkException extends Exception
{
	public InvalidMarkException(String message)
	{
		super(message);
	}
}
