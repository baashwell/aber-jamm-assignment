package uk.ac.aber.dcs.cs12420.aberjamm.data;

import java.io.Serializable;

/**
 * Assessment part is a part of each assessment, it contains the maximum mark for this part and also a description of
 * what it is. Contains methods to figure out if a given mark is valid.
 * @author Ben Ashwell
 *
 */
public class AssessmentPart implements Serializable
{
	//Attributes
	
	/** String to hold the description of the assessment part*/
	private String description;
	/** Integer to hold the maximum mark for this part*/
	private int maximumMark;
	
	//Constructors
	
	public AssessmentPart()
	{
	}
	
	public AssessmentPart(String desc, int max)
	{
		description = desc;
		maximumMark = max;
	}
	
	//Methods
	
	/**
	 * return the maximum mark of this assessment part
	 * @return
	 */
	public int getMaximumMark()
	{
		return maximumMark;
	}
	/**
	 * Set the maximumMark of this assessment part
	 * @param maximumMark
	 */
	public void setMaximumMark(int maximumMark)
	{
		this.maximumMark = maximumMark;
	}
	/**
	 * Check that the mark given is in-between 0 and the maximum value
	 * @param otherMark
	 * @return
	 */
	public boolean isValid(int otherMark)
	{
		if(otherMark >= 0 && otherMark <= maximumMark)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * @return the description
	 */
	public String getDescription() 
	{
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) 
	{
		this.description = description;
	}
}
