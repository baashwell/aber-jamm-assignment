package uk.ac.aber.dcs.cs12420.aberjamm.data;

//Imports
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.util.ArrayList;

/**
 * Module class is the basis of the AberJamm project, everything else leads off of this class.
 * It implements the serializable class so that it can save and load in modules. 
 * @author Ben Ashwell
 *
 */
public class Module implements Serializable
{
	//Attributes
	
	/** Sting to hold the module code*/
	private String moduleCode;
	/** Sting to hold the title*/
	private String title;
	/** Vector to hold the list of Assessments for the module */
	private ArrayList<Assessment> assessments  = new ArrayList<Assessment>();
	/** Vector to hold the list of Students on the module */
	private ArrayList<Student> students = new ArrayList<Student>();
	
	//Constructors
	
	public Module()
	{
	}
	
	public Module(String moduleCode)
	{
		this.moduleCode = moduleCode;
	}
	
	public Module(String moduleCode, String title)
	{
		this.moduleCode = moduleCode;
		this.title = title;
	}
	
	//Methods
	
	/**
	 * Add an Assessment to the Assessment list
	 * @param assessment
	 */
	public void addAssessment(Assessment assessment)
	{	
		assessments.add(assessment);
	}
	
	/**
	 * Get the number of Students in the Student list
	 * @return
	 */
	public int getNumberOfStudents()
	{	
		return students.size();
	}
	
	//will comment once done the catch!
	public void write(String filePath)
	{
		try 
		{
			XMLEncoder e = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(filePath + ".xml")));
			e.writeObject(this);
			e.close();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}								
	}
	
	//will comment later
	public Module read(String filePath)
	{
		try 
		{
			XMLDecoder d = new XMLDecoder(new BufferedInputStream(new FileInputStream(filePath)));
			Module m = (Module)d.readObject();
			d.close();
			return m;
			
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
			return null;
		}
		
	}
	/**
	 * method to add a student to the student list, here if only one student sent
	 * @param student
	 */
	public void setStudents(Student student)
	{
		students.add(student);
	}
	/**
	 * method to set the students list
	 * @param students
	 */
	public void setStudents(ArrayList<Student> students)
	{
		this.students.addAll(students);
	}

	/**
	 * @return the moduleCode
	 */
	public String getModuleCode() 
	{
		return moduleCode;
	}

	/**
	 * @param moduleCode the moduleCode to set
	 */
	public void setModuleCode(String moduleCode)
	{
		this.moduleCode = moduleCode;
	}

	/**
	 * @return the title
	 */
	public String getTitle() 
	{
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}

	/**
	 * @return the assessments
	 */
	public ArrayList<Assessment> getAssessments() 
	{
		return assessments;
	}

	/**
	 * @param assessments the assessments to set
	 */
	public void setAssessments(ArrayList<Assessment> assessments) 
	{
		this.assessments = assessments;
	}

	/**
	 * @return the students
	 */
	public ArrayList<Student> getStudents() 
	{
		return students;
	}
}
